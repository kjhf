/*****************************************************************************
**
**  hyperfuck -- boolfuck in the hyperbolic plane
**  Copyright (C) 2010 Claude Heiland-Allen <claude@mathr.co.uk>
**
*/

#ifndef BITSTREAM_H
#define BITSTREAM_H 1

/***    struct ibitstream    *************************************************
**
**  an input stream of bits
**
**  USAGE
**    i = i->get(i);  // get() invalidates its argument
**
*/

struct ibitstream {
  /* public, readonly */
  int end;
  int bit;
  struct ibitstream *(*get)(struct ibitstream *);
  /* private, implementation defined */
  void *data;
};

/***    struct obitstream    *************************************************
**
**  an output stream of bits
**
**  USAGE
**    o = o->put(o, b);  // put() invalidates its argument
**
*/

struct obitstream {
  /* public, readonly */
  struct obitstream *(*put)(struct obitstream *, int);
  /* private, implementation defined */
  void *data;
};

#endif

