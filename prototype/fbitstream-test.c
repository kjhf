/*****************************************************************************
**
**  hyperfuck -- boolfuck in the hyperbolic plane
**  Copyright (C) 2010 Claude Heiland-Allen <claude@mathr.co.uk>
**
*/

#include <stdlib.h>
#include "fbitstream.h"

/* cat */
int main(int argc, char **argv) {
  struct ibitstream *i = fibitstream(stdin);
  struct obitstream *o = fobitstream(stdout);
  do {
    i = i->get(i);
    if (!i->end) {
      o = o->put(o, i->bit);
    }
  } while (!i->end);
  return 0;
}

