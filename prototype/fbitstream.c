/*****************************************************************************
**
**  hyperfuck -- boolfuck in the hyperbolic plane
**  Copyright (C) 2010 Claude Heiland-Allen <claude@mathr.co.uk>
**
*/

#include <stdlib.h>
#include "fbitstream.h"

struct fileData {
  int bit;
  int byte;
  FILE *file;
};

static struct ibitstream *fibitstreamGet(struct ibitstream *bs) {
  if (!bs->end) {
    struct fileData *data = bs->data;
    if (data->bit == 0) {
      data->byte = fgetc(data->file);
      if (data->byte == EOF) {
        bs->end = 1;
        return bs;
      } else {
        data->bit = 8;
      }
    }
    data->bit--;
    bs->bit = (data->byte & (1 << data->bit)) >> data->bit;
  }
  return bs;
}

struct ibitstream *fibitstream(FILE *f) {
  struct ibitstream *bs = malloc(sizeof(struct ibitstream));
  bs->bit = 0;
  bs->end = 0;
  bs->get = &fibitstreamGet;
  struct fileData *data = malloc(sizeof(struct fileData));
  data->bit = 0;
  data->byte = 0;
  data->file = f;
  bs->data = data;
  return bs;  
}

static struct obitstream *fobitstreamPut(struct obitstream *bs, int b) {
  struct fileData *data = bs->data;
  data->bit--;
  data->byte |= (1 & b) << data->bit;
  if (data->bit == 0) {
    fputc(data->byte, data->file);
    fflush(data->file);
    data->bit = 8;
    data->byte = 0;
  }
  return bs;
}

struct obitstream *fobitstream(FILE *f) {
  struct obitstream *bs = malloc(sizeof(struct obitstream));
  bs->put = &fobitstreamPut;
  struct fileData *data = malloc(sizeof(struct fileData));
  data->bit = 8;
  data->byte = 0;
  data->file = f;
  bs->data = data;
  return bs;  
}

