/*****************************************************************************
**
**  hyperfuck -- boolfuck in the hyperbolic plane
**  Copyright (C) 2010 Claude Heiland-Allen <claude@mathr.co.uk>
**
*/

#ifndef FBITSTREAM_H
#define FBITSTREAM_H 1

#include <stdio.h>

#include "bitstream.h"

/***    struct ibitstream *fibitstream(FILE *)    ****************************
**
**  a stream of bits from a file (big-endian bytes)
**
*/

struct ibitstream *fibitstream(FILE *f);

/***    struct obitstream *fobitstream(FILE *)    ****************************
**
**  a stream of bits to a file (big-endian bytes)
**
*/

struct obitstream *fobitstream(FILE *f);

#endif

