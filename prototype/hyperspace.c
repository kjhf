/*****************************************************************************
**
**  hyperfuck -- boolfuck in the hyperbolic plane
**  Copyright (C) 2010 Claude Heiland-Allen <claude@mathr.co.uk>
**
*/

#include <assert.h>
#include <stdlib.h>
#include "hyperspace.h"

/***    struct cell *cell(void)    *******************************************
**
**  create a new cell
**
*/

struct cell *cell(void) {
  struct cell *c = malloc(sizeof(struct cell));
  c->up = c->left = c->right = c->downLeft = c->downRight = 0;
  c->data = 0;
  return c;
}

/***    struct space *space(struct bitstream *)    ***************************
**
**  create a new space
**
*/

struct space *space(struct ibitstream *bs) {
  struct space *s = malloc(sizeof(struct space));
  s->above = bs;
  s->top = cell();
  return s;
}

/***    struct cursor *cursor(struct space *)    *****************************
**
**  create a new cursor
**
*/

struct cursor *cursor(struct space *s) {
  struct cursor *c = malloc(sizeof(struct cursor));
  c->space = s;
  c->cell = s->top;
  return c;
}

/***    struct cursor *up(struct cursor *, int)    ***************************
**
**  move up
**
**  TOP                     LEFT                    RIGHT
**    left    right           (exists)                (as LEFT)
**    +---+   +---+
**    | N |   | N |
**    +-+-+   +-+-+
**    |O|       |O|
**    +++       +++
**
*/

struct cursor *up(struct cursor *o, int wantRight) {
  if (!o->cell->up) {
    int r;
    if (o->space->above->end) {
      r = wantRight;
    } else {
      o->space->above = o->space->above->get(o->space->above);
      r = o->space->above->bit;
    }
    o->cell->up = cell();
    if (r) { o->cell->up->downRight = o->cell; }
    else   { o->cell->up->downLeft  = o->cell; }
    o->space->top = o->cell->up;
  }
  o->cell = o->cell->up;
  return o;
}

/***    struct cursor *downLeft(struct cursor *)    **************************
**
**  move down left
**
**  TOP                     LEFT                    RIGHT
**    +---+                   +---+---+               (as LEFT)
**    | O |                   |???| O |
**    +-+-+                   +-+-+-+-+
**    |N|?|                     |?|N|?|
**    +++++                     +++++++
**
*/

struct cursor *downLeft(struct cursor *o) {
  if (!o->cell->downLeft) {
    o->cell->downLeft = cell();
    o->cell->downLeft->up = o->cell;
    if (o->cell->downRight) {
      o->cell->downRight->left = o->cell->downLeft;
      o->cell->downLeft->right = o->cell->downRight;
    }
    if (o->cell->left && o->cell->left->downRight) {
      o->cell->left->downRight->right = o->cell->downLeft;
      o->cell->downLeft->left = o->cell->left->downRight;
    }
  }
  o->cell = o->cell->downLeft;
  return o;
}

/***    struct cursor *downRight(struct cursor *)    *************************
**
**  move down right
**
**  TOP                     LEFT                    RIGHT
**    +---+                   +---+---+               (as LEFT)
**    | O |                   | O |???|
**    +-+-+                   +-+-+-+-+
**    |?|N|                   |?|N|?|
**    +++++                   +++++++
**
*/

struct cursor *downRight(struct cursor *o) {
  if (!o->cell->downRight) {
    o->cell->downRight = cell();
    o->cell->downRight->up = o->cell;
    if (o->cell->downLeft) {
      o->cell->downLeft->right = o->cell->downRight;
      o->cell->downRight->left = o->cell->downLeft;
    }
    if (o->cell->right && o->cell->right->downLeft) {
      o->cell->right->downLeft->left = o->cell->downRight;
      o->cell->downRight->right = o->cell->right->downLeft;
    }
  }
  o->cell = o->cell->downRight;
  return o;
}

/***    struct cursor *left(struct cursor *)    ******************************
**
**  move left
**
**  TOP                     LEFT                    RIGHT
**      +-------+             +---+---+               +---+---+
**      |   N   |             |???<###|               |???|###|
**      +-/-+-^-+             +-+\+^+-+               +-+-+/+^+
**    { | N | N | }           |?|N|O|                   |?|N|O|
**    { +-+\+^+-+ }           +++++++                   +++++++
**        |N|O|
**        +++++
**
*/

struct cursor *left(struct cursor *o) {
  if (!o->cell->left) {
    if        (o->cell->up && o->cell->up->downRight == o->cell) { /* RIGHT */
      o->cell = o->cell->up;
      o = downLeft(o);
      return o;
    } else if (o->cell->up && o->cell->up->downLeft  == o->cell) { /* LEFT */
      o->cell = o->cell->up;
      o = left(o);
      o = downRight(o);     
      return o;
    } else if (!o->cell->up) {                                     /* TOP */
      struct cell *c;
      int n = 0;
      do {
        c = o->cell;
        o = up(o, 1);
        n++;
      } while (o->cell->downLeft == c);
      o = downLeft(o);
      while (n--) {
        o = downRight(o);
      }
      return o;
    } else {
      assert(0); /* broken */
    }
  }
  o->cell = o->cell->left;
  return o;
}

/***    struct cursor *right(struct cursor *)    *****************************
**
**  move right
**
**  TOP                     LEFT                    RIGHT
**      +-------+             +---+---+               +---+---+
**      |   N   |             |###|???|               |###>###|
**      +-^-+-\-+             +^+\+-+-+               +-+^+/+-+
**    { | N | N | }           |O|N|?|                   |O|N|?|
**    { +-+^+/+-+ }           +++++++                   +++++++
**        |O|N|
**        +++++
**
*/

struct cursor *right(struct cursor *o) {
  if (!o->cell->right) {
    if        (o->cell->up && o->cell->up->downRight == o->cell) { /* RIGHT */
      o->cell = o->cell->up;
      o = right(o);
      o = downLeft(o);
      return o;
    } else if (o->cell->up && o->cell->up->downLeft  == o->cell) { /* LEFT */
      o->cell = o->cell->up;
      o = downRight(o);
      return o;
    } else if (!o->cell->up) {                                     /* TOP */
      struct cell *c;
      int n = 0;
      do {
        c = o->cell;
        o = up(o, 0);
        n++;
      } while (o->cell->downRight == c);
      o = downRight(o);
      while (n--) {
        o = downLeft(o);
      }
      return o;
    } else {
      assert(0); /* broken */
    }
  }
  o->cell = o->cell->right;
  return o;
}

