/*****************************************************************************
**
**  hyperfuck -- boolfuck in the hyperbolic plane
**  Copyright (C) 2010 Claude Heiland-Allen <claude@mathr.co.uk>
**
*/

#ifndef HYPERSPACE_H
#define HYPERSPACE_H 1

#include "bitstream.h"

/***    struct cell    *******************************************************
**
**  pentagonal hyperbolic cell data structure definition
**
**  TOP                     LEFT                    RIGHT
**    c->up == 0              c->up != 0              c->up != 0
**                            c->up->downLeft == c    c->up->downRight == c
**                              +-------+             +-------+
**        0                     |   ^   |             |   ^   |
**      +---+                   +---+---+             +---+---+
**    0 |###| 0               ?<|###|>?                 ?<|###|>?
**      +-+-+                   +-+-+                     +-+-+
**       / \                     / \                       / \
**      ?   ?                   ?   ?                     ?   ?
**
**  INVARIANT
**    exactly one cell is TOP
**
*/

struct cell {
  struct cell *up, *left, *right, *downLeft, *downRight;
  void *data;
};

struct cell *cell(void);

/***    struct space    ******************************************************
**
**  a region of hyperbolic space defined by its top cell below a bitstream
**
**  INVARIANT
**    s->above is finite, OR,
**    every suffix of s->above contains a 0 and a 1
**
**  RATIONALE
**    an infinite stream of 0 will cause problems when moving left
**    an infinite stream of 1 will cause problems when moving right
**
*/

struct space {
  struct ibitstream *above;
  struct cell *top;
};

struct space *space(struct ibitstream *);

/***    struct cursor    *****************************************************
**
**  a point in a region of hyperbolic space
**  moving a cursor expands the region as required
**
*/

struct cursor {
  struct space *space;
  struct cell *cell;
};

struct cursor *cursor(struct space*);

struct cursor *up       (struct cursor *, int);
struct cursor *left     (struct cursor *);
struct cursor *right    (struct cursor *);
struct cursor *downLeft (struct cursor *);
struct cursor *downRight(struct cursor *);

#endif

