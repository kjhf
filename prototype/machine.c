/*****************************************************************************
**
**  hyperfuck -- boolfuck in the hyperbolic plane
**  Copyright (C) 2010 Claude Heiland-Allen <claude@mathr.co.uk>
**
*/

#include <stdio.h>
#include <stdlib.h>
#include "machine.h"
#include "fbitstream.h"
#include "parser.h"

static void *zero = 0;
static void *one = &one;

struct machine *machine(char *c, FILE *g, FILE *i, FILE *o) {
  struct machine *m = malloc(sizeof(struct machine));
  m->input = fibitstream(i);
  m->output = fobitstream(o);
  m->data = space(fibitstream(g));
  m->dataPtr = cursor(m->data);
  m->code = parse(c);
  m->codePtr = m->code;
  m->running = 1;
  return m;
}

struct machine *step(struct machine *m) {
  int bit = m->dataPtr->cell->data != zero;
  enum opcode op = m->codePtr->op;
  /* fputc("+^<>/\\,;|#"[op], stderr); */
  switch (op) {
  case (oHalt):      m->running = 0; break;
  case (oFlip):      m->dataPtr->cell->data = !bit ? one : zero; break;
  case (oUp):        m->dataPtr = up       (m->dataPtr, 0); break;
  case (oLeft):      m->dataPtr = left     (m->dataPtr); break;
  case (oRight):     m->dataPtr = right    (m->dataPtr); break;
  case (oDownLeft):  m->dataPtr = downLeft (m->dataPtr); break;
  case (oDownRight): m->dataPtr = downRight(m->dataPtr); break;
  case (oInput):     m->input = m->input->get(m->input);
                     m->dataPtr->cell->data = m->input->bit ? one : zero;
                     break;
  case (oOutput):    m->output = m->output->put(m->output, bit);
                     if (m->input->end) { m->dataPtr->cell->data = zero; }
                     break;
  default:           break;
  }
  m->codePtr = m->codePtr->next[bit];
  return m;
}

