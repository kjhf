/*****************************************************************************
**
**  hyperfuck -- boolfuck in the hyperbolic plane
**  Copyright (C) 2010 Claude Heiland-Allen <claude@mathr.co.uk>
**
*/

#ifndef MACHINE_H
#define MACHINE_H 1

#include <stdio.h>
#include "hyperspace.h"

struct machine {
  struct ibitstream *input;
  struct obitstream *output;
  struct space *data;
  struct cursor *dataPtr;
  struct instruction *code;
  struct instruction *codePtr;
  int running;
};

struct machine *machine(char *, FILE *, FILE *, FILE *);
struct machine *step(struct machine *);

#endif

