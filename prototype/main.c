/*****************************************************************************
**
**  hyperfuck -- boolfuck in the hyperbolic plane
**  Copyright (C) 2010 Claude Heiland-Allen <claude@mathr.co.uk>
**
*/

#include <assert.h>
#include <stdio.h>
#include "machine.h"

int main(int argc, char **argv) {
  assert(argc > 1);
  char *source = argv[1];
  FILE *geometry = fopen("kjv10.txt", "rb");
  assert(geometry);
  struct machine *m = machine(source, geometry, stdin, stdout);
  while (m->running) { m = step(m); }
  return 0;
}

