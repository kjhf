/*****************************************************************************
**
**  hyperfuck -- boolfuck in the hyperbolic plane
**  Copyright (C) 2010 Claude Heiland-Allen <claude@mathr.co.uk>
**
*/

#include <assert.h>
#include <stdlib.h>
#include "parser.h"

struct instruction *parse(char *s) {
  /* pass 1: instruction allocation */
  struct instruction *pc = malloc(sizeof(struct instruction));
  struct instruction *pc0 = pc;
  struct instruction *pc1, *pc2;
  int depth;
  do {
    switch (*s) {
    case ('\0'): pc->op = oHalt; pc->next[0] = pc->next[1] = 0; break;
#define OP(C,X) case (C): pc->op = (X); pc = pc->next[0] = pc->next[1] = malloc(sizeof(struct instruction)); break
    OP('+', oFlip);
    OP('^', oUp);
    OP('<', oLeft);
    OP('>', oRight);
    OP('/', oDownLeft);
    OP('\\',oDownRight);
    OP(',', oInput);
    OP(';', oOutput);
    OP('[', oBra);
    OP(']', oKet);
#undef OP
    }
  } while (*s++);
  /* pass 2: bracket fixating, eg:  [+[+]+]
      ________________________________________
     /          __________________            |
    /          /                  |           |
    skip,-flip->skip,->flip->skip<            |
        |           |_____________>flip->skip<
        |_____________________________________>END
  */
  pc = pc0;
  while (pc) {
    pc2 = pc->next[0];
    assert(pc->op != oKet);
    if (pc->op == oBra) {
      pc1 = pc->next[0];
      depth = 1;
      while (depth > 0) {
        if        (pc1->op == oBra) {
          depth++;
          pc1 = pc1->next[0];
        } else if (pc1->op == oKet) {
          depth--;
          if (depth > 0) {
            pc1 = pc1->next[0];
          }
        } else {
          pc1 = pc1->next[0];
        }
      }
      assert(depth == 0 && pc1 && pc1->op == oKet);
      pc1->next[1] = pc->next[0];
      pc->next[0] = pc1->next[0];
      pc->op = pc1->op = oSkip;
    }
    pc = pc2;
  }
  return pc0;
}

