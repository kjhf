/*****************************************************************************
**
**  hyperfuck -- boolfuck in the hyperbolic plane
**  Copyright (C) 2010 Claude Heiland-Allen <claude@mathr.co.uk>
**
*/

#ifndef PARSER_H
#define PARSER_H 1

enum opcode {
  oFlip,
  oUp, oLeft, oRight, oDownLeft, oDownRight,
  oInput, oOutput,
  oSkip,
  oHalt,
  oBra, oKet
};

struct instruction {
  enum opcode op;
  struct instruction *next[2];
};

struct instruction *parse(char *);

#endif

