module DataTape(DataTape(), dataTape, get, modify, up, left, right, downLeft, downRight, isRightChild, getPointer, pretty) where

import Data.List(intersperse)
import Data.Map(Map)
import qualified Data.Map as M

default (Int)

data Cell = Cell{ cUp, cLeft, cRight, cDownLeft, cDownRight :: Maybe Int, cData :: Bool }
  deriving (Read, Show)

cell :: Cell
cell = Cell Nothing Nothing Nothing Nothing Nothing False

data DataTape = DataTape{ dtCells :: Map Int Cell, dtPtr :: Int, _dtPosition :: [Bool] }

dataTape :: [Bool] -> DataTape
dataTape ps = DataTape (M.singleton 0 cell) 0 ps

get :: DataTape -> Bool
get (DataTape m p _) = cData (m M.! p)

modify :: (Bool -> Bool) -> DataTape -> DataTape
modify f dt@(DataTape m p _) = dt{ dtCells = M.update (\c -> Just c{ cData = f (cData c) }) p m }

merge :: Int -> Int -> DataTape -> DataTape
merge l r dt = dt{ dtCells = M.update (\c -> Just c{ cLeft  = Just l }) r
                           . M.update (\c -> Just c{ cRight = Just r }) l
                           $ dtCells dt }

isRightChild :: DataTape -> Bool
isRightChild dt = let dt' = downRight . up $ dt
                  in dtPtr dt == dtPtr dt'

getPointer :: DataTape -> Int
getPointer dt = dtPtr dt

up :: DataTape -> DataTape
up dt@(DataTape m p (b:bs)) = case cUp (m M.! p) of
  Just q  -> dt{ dtPtr = q }
  Nothing -> let q = M.size m
                 c = if b then cell{ cDownRight = Just p } else cell{ cDownLeft = Just p }
                 n = M.update (\c0 -> Just c0{ cUp = Just q }) p m
             in DataTape (M.insert q c n) q bs
up    (DataTape _ _ []    ) = error "DataTape.up: no more position data"

left :: DataTape -> DataTape
left dt@(DataTape m p _) = case cLeft (m M.! p) of
  Just q  -> dt{ dtPtr = q }
  Nothing -> let dt'@(DataTape m' u _) = up dt
             in if cDownRight (m' M.! u) == Just p then                                        downLeft         $ dt' else
                if cDownLeft  (m' M.! u) == Just p then (\dt'' -> merge (dtPtr dt'') p dt'') . downRight . left $ dt' else
                error "DataTape.left: internal error: went up but no child"

right :: DataTape -> DataTape
right dt@(DataTape m p _) = case cRight (m M.! p) of
  Just q  -> dt{ dtPtr = q }
  Nothing -> let dt'@(DataTape m' u _) = up dt
             in if cDownLeft  (m' M.! u) == Just p then                                        downRight        $ dt' else
                if cDownRight (m' M.! u) == Just p then (\dt'' -> merge p (dtPtr dt'') dt'') . downLeft . right $ dt' else
                error "DataTape.right: internal error: went up but no child"

downLeft :: DataTape -> DataTape
downLeft dt@(DataTape m p _) = case cDownLeft (m M.! p) of
  Just q  -> dt{ dtPtr = q }
  Nothing -> let q = M.size m
                 c = cell{ cUp = Just p }
                 n = M.update (\c0 -> Just c0{ cDownLeft = Just q }) p m
             in maybe id (\r -> merge q r) (cDownRight (m M.! p)) dt{ dtCells = M.insert q c n, dtPtr = q }

downRight :: DataTape -> DataTape
downRight dt@(DataTape m p _) = case cDownRight (m M.! p) of
  Just q  -> dt{ dtPtr = q }
  Nothing -> let q = M.size m
                 c = cell{ cUp = Just p }
                 n = M.update (\c0 -> Just c0{ cDownRight = Just q }) p m
             in maybe id (\l -> merge l q) (cDownLeft (m M.! p)) dt{ dtCells = M.insert q c n, dtPtr = q }

pretty :: DataTape -> String
pretty dt = pretty' 3 (5 * 2^(8 - 3 :: Int) + 2) (dtPtr dt) (left . left . left . left . left $ dt)

pretty' :: Int -> Int -> Int -> DataTape -> String
pretty' n dx p0 dt | n >  0 = let dt' = up dt
                                  isR = cDownRight (dtCells dt' M.! dtPtr dt') == Just (dtPtr dt)
                                  dx' = dx + (if isR then id else negate) (2 ^ (7 - n)) - 1
                              in pretty' (n - 1) dx' p0 dt'
                   | otherwise = let w = 2^(8 :: Int) + 1
                                 in unlines . (++ [replicate w '+']) . concat . zipWith inflate [5,4 ..] . map (take w) . map (drop dx) . drop 1 . take 8 . pretty'' 7 p0 $ dt

inflate :: Int -> String -> [String]
inflate n l | n >= -1 = let h = map (\c -> if c == '|' then '+' else '-') l
                            i = map (\c -> if c == '|' then '|' else ' ') l
                            is = drop 1 (replicate (2^(n+1) `div` 4) i)
                        in [h] ++ is ++ [l] ++ is
            | otherwise = error "DataTape.inflate: n < -1"

pretty'' :: Int -> Int -> DataTape -> [String]
pretty'' n p0 dt = (concat . intersperse "|" . map (pretty''' n p0) . iterate right $ dt) : pretty'' (n - 1) p0 (downLeft dt)

pretty''' :: Int -> Int -> DataTape -> String
pretty''' n p0 dt | dtPtr dt == p0 = replicate (2^n-2) ' ' ++ "[" ++ (if get dt then "1" else "0") ++ "]" ++ replicate (2^n-2) ' '
                  | otherwise      = replicate (2^n-1) ' ' ++        (if get dt then "1" else "0")        ++ replicate (2^n-1) ' '
