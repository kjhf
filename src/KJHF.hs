module Main(main) where

import Graphics.UI.GLUT hiding (get, cursor)
import Sound.OSC.Time(time)

import Data.IORef
import Data.Char(ord, chr)
import Data.Bits(testBit, shiftR)
import Control.Monad(when)
import Network(connectTo, PortID(PortNumber))
import System.IO(stdout, openFile, withBinaryFile, IOMode(ReadMode, WriteMode), hGetBuf, hSetBuffering, BufferMode(LineBuffering), hPutStrLn, Handle)
import System.Exit(exitFailure)
import Foreign.Marshal.Alloc(allocaBytes)

import Machine   hiding (pretty)
import DataTape  hiding (pretty)
import Snapshot

rt :: Bool
rt = True

data World = World
  { wMachine, wNextMachine :: Machine
  , wNow :: Double
  , wCorners :: (GLdouble, GLdouble, GLdouble, GLdouble)
  , wScale :: GLdouble
  , wTranslate :: GLdouble
  , wDelta :: GLdouble
  , wOutput :: [Bool]
  , wOutputText :: [Char]
  , fontTex :: TextureObject
  , tileTex :: TextureObject
  , codeTex :: TextureObject
  , wPd :: Handle
  , wFrames :: Int
  }

reshape :: IORef World -> Size -> IO ()
reshape worldRef vp@(Size w h) = do
  let s = 8 * 12 / 2
      cs@(x0,x1,y0,y1) = let v = s * fromIntegral w / fromIntegral h
                         in (-v,v,0,2*s)
  writeIORef worldRef . (\ww-> ww{ wCorners = cs }) =<< readIORef worldRef
  viewport $= (Position 0 0, vp)
  matrixMode $= Projection
  loadIdentity
  ortho x0 x1 y0 y1 (-1) 1
  matrixMode $= Modelview 0
  loadIdentity
  postRedisplay Nothing

clock :: Double
clock = 0.24

simulate :: Int -> IORef World -> IO ()
simulate mspf worldRef = do
  when (not rt) $
    writeIORef worldRef . (\ww -> ww{ wFrames = wFrames ww + 1 }) =<< readIORef worldRef
  world <- readIORef worldRef
  tnow <- if rt then time else return (0.04 * fromIntegral (wFrames world) + wNow world)
  let t  = (tnow - wNow world) / clock
  when (t >= 1) $ do
    case step (wNextMachine world) of
      (Just (mach, op), out) -> do
        let ds = case op of
                   OpUp        -> 0.5
                   OpDownLeft  -> 2.0
                   OpDownRight -> 2.0
                   _           -> 1.0
            dx = case op of
                   OpUp        -> if isRightChild (getData (wMachine world))
                                  then  6.0
                                  else -6.0
                   OpLeft      ->  12.0
                   OpRight     -> -12.0
                   OpDownLeft  ->   3.0
                   OpDownRight ->  -3.0
                   _           ->   0.0
            os = case out of
                   Just b  -> b : wOutput world
                   Nothing ->     wOutput world
            (c, os') = if length os >= 8
                       then (Just . littleChar . take 8 $ os, drop 8 os)
                       else (Nothing, os)
        hPutStrLn (wPd world) ("op " ++ show (fromEnum op) ++ ";")
        case c of
          Just c' ->  writeIORef worldRef world
            { wMachine = wNextMachine world
            , wNextMachine = mach
            , wNow = tnow
            , wScale = ds
            , wTranslate = (dx / 6)
            , wOutput = os'
            , wOutputText = c' : take 31 (wOutputText world)
            , wFrames = 0
            }
          Nothing ->  writeIORef worldRef world
            { wMachine = wNextMachine world
            , wNextMachine = mach
            , wNow = tnow
            , wScale = ds
            , wTranslate = (dx / 6)
            , wOutput = os'
            , wFrames = 0
            }
      _ -> writeIORef worldRef world{ wScale = 1.0, wTranslate = 0.0 }
  world' <- readIORef worldRef
  writeIORef worldRef world'
    { wDelta = realToFrac ((tnow - wNow world') / clock) }
  postRedisplay Nothing
  addTimerCallback mspf $ simulate mspf worldRef

display :: IORef World -> IO ()
display worldRef = do
  w <- readIORef worldRef
  let dt = getData . wMachine $ w
      (x0,x1,y0,y1) = wCorners w
      ox0 = (x0   +   x1) / 2
      oy0 = (y0*3 + 1*y1) / 4
      d = wDelta w
      ds = wScale w ** d
      dx = wTranslate w * (d ** (3/2))
      ox = ds * ox0 + dx * oy
      oy = ds * oy0
      gw = 10
      gh = 20
      textCoords = drop 1 $ [x1, x1 - gw ..] `zip` repeat y1
  -- setup
  clear [ColorBuffer]
  blend $= Enabled
  blendFunc $= (SrcAlpha, OneMinusSrcAlpha)
  texture Texture2D $= Enabled
  -- draw tiles
  textureBinding Texture2D $= Just (tileTex w)
  renderPrimitive Quads $
    mapM_ drawTile . concat . take 10 .
    map (takeWhile (not . farRight x1) . dropWhile (farLeft x0)) .
    dropWhile (farAbove y1 . head) . toTileRows (getPointer dt) .
    toTopLeft (x0, y1) (ox, oy) $ dt
  -- draw text
  textureBinding Texture2D $= Just (fontTex w)
  renderPrimitive Quads $
    mapM_ (drawGlyph gw gh) . zip textCoords . take 32 $ wOutputText w
  -- draw code
  let cw = 20
      ch = 20
      cy = y1 - gh
      codeLeftCoords  = drop 2 $ [cw/2, cw/2 - cw ..] `zip` repeat cy
      codeRightCoords =          [cw/2 - cw, cw/2 ..] `zip` repeat cy
      (codeLeft, codeRight) = getCode (wMachine w)
  textureBinding Texture2D $= Just (codeTex w)
  renderPrimitive Quads $ do
    mapM_ (drawCode cw ch) . zip codeLeftCoords  $ codeLeft
    mapM_ (drawCode cw ch) . zip codeRightCoords $ codeRight
  -- cleanup
  textureBinding Texture2D $= Nothing
  texture Texture2D $= Disabled
  swapBuffers
  when (not rt) $ hSnapshot stdout (Position 0 0) (Size 880 480)

drawGlyph :: GLdouble -> GLdouble -> ((GLdouble,GLdouble), Char) -> IO ()
drawGlyph w h ((x,y), c) = do
  let n = ord c
      tx0 :: GLdouble
      tx0 = fromIntegral (n `mod` 16    ) *  64 / 1024
      ty0 = fromIntegral (n `div` 16    ) * 128 / 1024
      tx1 = fromIntegral (n `mod` 16 + 1) *  64 / 1024
      ty1 = fromIntegral (n `div` 16 + 1) * 128 / 1024
  color $ Color3 1 1 (1::GLdouble)
  texCoord $ TexCoord2 tx0 ty0
  vertex $ Vertex2 x y
  texCoord $ TexCoord2 tx1 ty0
  vertex $ Vertex2 (x+w) y
  texCoord $ TexCoord2 tx1 ty1
  vertex $ Vertex2 (x+w) (y-h)
  texCoord $ TexCoord2 tx0 ty1
  vertex $ Vertex2 x (y-h)
  
drawCode :: GLdouble -> GLdouble -> ((GLdouble,GLdouble), Opcode) -> IO ()
drawCode w h ((x,y), c) = do
  let n = fromEnum c
      tx0 :: GLdouble
      tx0 = fromIntegral (n `mod` 4    ) * 256 / 1024
      ty0 = fromIntegral (n `div` 4    ) * 256 / 1024
      tx1 = fromIntegral (n `mod` 4 + 1) * 256 / 1024
      ty1 = fromIntegral (n `div` 4 + 1) * 256 / 1024
  color $ Color3 1 1 (1::GLdouble)
  texCoord $ TexCoord2 tx0 ty0
  vertex $ Vertex2 x y
  texCoord $ TexCoord2 tx1 ty0
  vertex $ Vertex2 (x+w) y
  texCoord $ TexCoord2 tx1 ty1
  vertex $ Vertex2 (x+w) (y-h)
  texCoord $ TexCoord2 tx0 ty1
  vertex $ Vertex2 x (y-h)
  
data Tile = Tile{ tcx, tcy :: GLdouble, tdata :: Bool, tcursor :: Bool }

drawTile :: Tile -> IO ()
drawTile t = do
  let cx = tcx t
      cy = tcy t
      y0 = cy *  4 / 6
      y1 = cy * 10 / 6
      x0t = cx - cy
      x1t = cx + cy
      x0b = cx - cy
      x1b = cx + cy
      c = case (tdata t, tcursor t) of
        (False, False) -> Color3 (176/255) (128/255) ( 80/255)
        (False, True ) -> Color3 (255/255) (186/255) (116/255)
        (True,  False) -> Color3 (168/255) ( 40/255) ( 16/255)
        (True,  True ) -> Color3 (255/255) ( 64/255) ( 24/255 :: GLdouble)
  color c
  texCoord $ TexCoord2 0 (1::GLdouble)
  vertex $ Vertex2 x0b y0
  texCoord $ TexCoord2 0 (0::GLdouble)
  vertex $ Vertex2 x0t y1
  texCoord $ TexCoord2 1 (0::GLdouble)
  vertex $ Vertex2 x1t y1
  texCoord $ TexCoord2 1 (1::GLdouble)
  vertex $ Vertex2 x1b y0

toTileRows :: Int -> (DataTape, (GLdouble, GLdouble)) -> [[Tile]]
toTileRows cursor dtcs = map (toTileRow cursor) . iterate downLeft' $ dtcs
  where downLeft' (dt, (cx, cy)) = (downLeft dt, (cx - cy / 2, cy / 2))

toTileRow :: Int -> (DataTape, (GLdouble, GLdouble)) -> [Tile]
toTileRow cursor dtcs = map (toTile cursor) . iterate right' $ dtcs
  where right' (dt, (cx, cy)) = (right dt, (cx + cy * 2, cy))
  
toTile :: Int -> (DataTape, (GLdouble, GLdouble)) -> Tile
toTile cursor (dt, (cx, cy)) = Tile
  { tcx = cx, tcy = cy, tdata = get dt, tcursor = cursor == getPointer dt }

toTopLeft :: (GLdouble, GLdouble) -> (GLdouble, GLdouble) -> DataTape
          -> (DataTape, (GLdouble, GLdouble))
toTopLeft xy@(x, y) cxy@(cx, cy) dt
  | cy * 2 / 3 < y = toTopLeft xy (if isRightChild dt
                                   then cx - cy
                                   else cx + cy, cy * 2) (up dt)
  | cx + cy    > x = toTopLeft xy (cx - 2 * cy, cy) (left dt)
  | otherwise      = (dt, cxy)

farAbove, farRight, farLeft :: GLdouble -> Tile -> Bool
farAbove y t = y < tcy t * 2 / 3
farRight x t = x < tcx t - tcy t
farLeft  x t = x > tcx t + tcy t


main :: IO ()
main = do
  let w = 880
      h = 480
  initialWindowSize $= Size w h
  initialDisplayMode $= [RGBAMode, DoubleBuffered]
  (_, code:_) <- getArgsAndInitialize
  _ <- createWindow "KJHF"
  tnow <- time
  dpos <- readBits "kjhf.dat"
  inpu <- readBits "kjhf.in"
  fontT <- loadTexture "font.rgba"
  tileT <- loadTexture "tile.rgba"
  codeT <- loadTexture "code.rgba"
  pd <- if rt then connectTo "localhost" (PortNumber 6666) else openFile "kjhf.score" WriteMode
  hSetBuffering pd LineBuffering
  let Just mach = machine code
                   (dpos ++ strBits (cycle " Forever and ever, amen.")) inpu
      world = World
                { wMachine = mach
                , wNextMachine = mach
                , wNow = tnow
                , wDelta = 0
                , wScale = 1
                , wTranslate = 0
                , wOutput = []
                , wOutputText = []
                , wCorners = (0, fromIntegral w, 0, fromIntegral h)
                , fontTex = fontT
                , tileTex = tileT
                , codeTex = codeT
                , wPd = pd
                , wFrames = 0
                }
  worldRef <- newIORef world
  displayCallback $= display worldRef
  reshapeCallback $= Just (reshape worldRef)
  let mspf = floor (1000 / 25 :: Double)
  addTimerCallback mspf $ simulate mspf worldRef
  mainLoop

loadTexture :: FilePath -> IO TextureObject
loadTexture f = do
  withBinaryFile f ReadMode $ \h -> do
    let count = 1024 * 1024 * 4
    allocaBytes count $ \pixels -> do
      count' <- hGetBuf h pixels count
      when (count' /= count) exitFailure
      [tex] <- genObjectNames 1
      texture Texture2D $= Enabled
      textureBinding Texture2D $= Just tex
      build2DMipmaps Texture2D RGBA' 1024 1024
        (PixelData RGBA UnsignedByte pixels)
      textureBinding Texture2D $= Nothing
      texture Texture2D $= Disabled
      return tex

strBits :: String -> [Bool]
strBits = concatMap (take 8 . littleBits . ord)

readBits :: FilePath -> IO [Bool]
readBits f = return . strBits =<< readFile f

littleBits :: Int -> [Bool]
littleBits = map (`testBit` 0) . iterate (`shiftR` 1)

littleChar :: [Bool] -> Char
littleChar = chr . foldl (\i b -> i * 2 + if b then 1 else 0) 0
