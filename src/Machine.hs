module Machine(Machine(), Opcode(..), machine, step, pretty, run, getCode, getData) where

import Data.Maybe(catMaybes, listToMaybe)

import qualified DataTape as D

swap :: (a, b) -> (b, a)
swap p = (snd p, fst p)

data Opcode = OpToggle | OpUp | OpLeft | OpRight | OpDownLeft | OpDownRight | OpInput | OpOutput | OpLoopStart | OpLoopEnd
  deriving (Read, Show, Eq, Enum)

opcodes :: [(Char, Opcode)]
opcodes = "+^<>/\\,;[]" `zip` [OpToggle, OpUp, OpLeft, OpRight, OpDownLeft, OpDownRight, OpInput, OpOutput, OpLoopStart, OpLoopEnd]

opchars :: [(Opcode, Char)]
opchars = map swap opcodes

parse :: String -> [Opcode]
parse = catMaybes . map (`lookup` opcodes)

prettyOp :: [Opcode] -> String
prettyOp = catMaybes . map (`lookup` opchars)

data Instruction = InOp Opcode | InLoop [Instruction]
  deriving (Read, Show, Eq)

compile :: [Opcode] -> Maybe [Instruction]
compile = fixup . foldr compile' (Just [[]])
  where
    compile' OpLoopEnd   (Just (     ls)) = Just $             []  : ls
    compile' OpLoopStart (Just (l:l':ls)) = Just $ (InLoop l : l') : ls
    compile' op          (Just (l   :ls)) = Just $ (InOp  op : l ) : ls
    compile' _           _                = Nothing
    fixup (Just [is]) = Just is
    fixup _           = Nothing

prettyIn :: [Instruction] -> String
prettyIn [] = []
prettyIn (InOp  op : is) = prettyOp [op] ++ prettyIn is
prettyIn (InLoop l : is) = "[" ++ prettyIn l ++ "]" ++ prettyIn is

data CodeTape = CodeTape{ ctParent :: Maybe CodeTape, ctLeft :: [Instruction], ctRight :: [Instruction] }
  deriving (Read, Show)

codeTape :: [Instruction] -> CodeTape
codeTape is = CodeTape Nothing [] is

getCode :: Machine -> ([Opcode], [Opcode])
getCode m = let (ls, rs) = getCode' ([], []) (mCode m) in (reverse ls, rs)

getCode' :: ([Opcode], [Opcode]) -> CodeTape -> ([Opcode], [Opcode])
getCode' (ls, rs) ct = case ctParent ct of
  Nothing ->          (                 ops (reverse $ ctLeft ct) ++ ls, rs ++ ops (ctRight ct))
  Just pt -> getCode' ([OpLoopStart] ++ ops (reverse $ ctLeft ct) ++ ls, rs ++ ops (ctRight ct) ++ [OpLoopEnd]) pt{ ctRight = drop 1 (ctRight pt) }

ops :: [Instruction] -> [Opcode]
ops = concatMap op1

op1 :: Instruction -> [Opcode]
op1 (InOp   o) = [o]
op1 (InLoop l) = [OpLoopStart] ++ ops l ++ [OpLoopEnd]

prettyCT :: CodeTape -> String
prettyCT ct = prettyCT' "#" ct

prettyCT' :: String -> CodeTape -> String
prettyCT' s ct = case ctParent ct of
  Nothing -> prettyIn (reverse (ctLeft ct)) ++ s ++ prettyIn (ctRight ct)
  Just pt -> prettyCT' ("{[" ++ prettyCT' s ct{ ctParent = Nothing } ++ "]}") pt{ ctRight = tail (ctRight pt) }

data Machine = Machine{ mCode :: CodeTape, mData :: D.DataTape, mInput :: [Bool] }

machine :: String -> [Bool] -> [Bool] -> Maybe Machine
machine code dpos input = case compile . parse $ code of
  Just is -> Just Machine{ mCode = codeTape is, mData = D.dataTape dpos, mInput = input }
  Nothing -> Nothing

step :: Machine -> (Maybe (Machine, Opcode), Maybe Bool)
step m = case nextIn m of
  Just (InOp o) -> let md f = (Just (m{ mData = f (mData m) }, o), Nothing)
                   in advance' $ case o of
    OpToggle     -> md (D.modify not)
    OpUp         -> md D.up
    OpLeft       -> md D.left
    OpRight      -> md D.right
    OpDownLeft   -> md D.downLeft
    OpDownRight  -> md D.downRight
    OpInput      -> case (mInput m) of
      []     -> (Just (m, o), Nothing)
      (i:is) -> (Just (m{ mData = D.modify (const i) (mData m), mInput = is }, o), Nothing)
    OpOutput     -> let b = D.get (mData m)
                        f = if null (mInput m) then D.modify (const False) else id
                    in (fst (md f), Just b)
    _            -> error "Machine.step: internal error: unexpected opcode"
  Just (InLoop is) -> let b = D.get (mData m)
                      in if b then (Just (m{ mCode = (codeTape is){ ctParent = Just (mCode m) } }, OpLoopStart), Nothing)
                              else advance' (Just (m, OpLoopStart), Nothing)
  Nothing -> case ctParent (mCode m) of
    Nothing -> (Nothing, Nothing)
    Just cp -> (Just (m{ mCode = cp }, OpLoopEnd), Nothing)
  where
    advance' :: (Maybe (Machine, Opcode), Maybe Bool) -> (Maybe (Machine, Opcode), Maybe Bool)
    advance' (Just (m', o'), out) = (Just (m'{ mCode = advance (mCode m') }, o'), out)
    advance' (Nothing,       out) = (Nothing, out)

nextIn :: Machine -> Maybe Instruction
nextIn m = listToMaybe (ctRight (mCode m))

advance :: CodeTape -> CodeTape
advance (CodeTape p ls (r:rs)) = CodeTape p (r:ls) rs
advance (CodeTape _ _  [])     = error "Machine.advance: internal error: no more code"

pretty :: Machine -> String
pretty m = D.pretty (mData m) ++ prettyCT (mCode m) ++ map (\b -> if b then '1' else '0') (take 72 (mInput m))

run :: Machine -> ([Machine], [Bool])
run m = case step m of
  (Nothing, _) -> ([m], [])
  (Just (m', _), o) -> let (ms, os) = run m' in (m : ms, maybe os (:os) o)

getData :: Machine -> D.DataTape
getData m = mData m
