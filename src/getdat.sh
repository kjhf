#!/bin/bash
touch kjhf.dat &&
mv -fv kjhf.dat kjhf.dat.bak.$$ &&
wget -N -c "http://www.gutenberg.org/ebooks/10.txt.utf-8" &&
cat 10.txt.utf-8.patch.gz | gunzip |
patch -p1 -o - 10.txt.utf-8 - |
sed "s|\r||g" > kjhf.dat
