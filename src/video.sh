#!/bin/bash
./KJHF '+[^+\;^]' |
ppmtoy4m -S 444 -F 25:1 |
y4mscaler -I sar=1/1 -O preset=dvd_wide -O yscale=1/1 |
mpeg2enc -f 8 -q 3 -b 8000 -B 768 -D 9 -g 9 -G 15 -P -R 2 -o kjhf.m2v
# TODO: render kjhf.score to kjhf.wav using Pd batch mode
# twolame -b 224 kjhf.wav &&
# mplex -f 8 -V -o kjhf.mpeg kjhf.m2v kjhf.mp2 &&
# rm kjhf.wav kjhf.mp2 kjhf.m2v kjhf.score &&
# ffmpeg2theora -p preview kjhf.mpeg
